<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
<!-- Header meta -->
@include('layouts.header_meta')

<!-- Fonts -->
@include('layouts.header_fonts')

<!-- Styles -->
@include('layouts.header_styles')
@yield('header.styles')

<!-- Scripts -->
@include('layouts.header_scripts')
@yield('header.scripts')
</head>
<body>

<!-- Navigation -->
<header>
    @include('layouts.navigation')
</header>

<!-- Content -->
<main>
    @yield('content')
</main>

<!-- Footer -->
<footer class="dark-bg">
    @include('layouts.footer')
</footer>

<!-- Footer scripts -->
@include('layouts.footer_scripts')
@yield('footer.scripts')
</body>
</html>
