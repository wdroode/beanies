<!-- Meta -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="author" content="{{ config('app.author') }}">

<title>{{ config('app.name') }}</title>

<!-- Favicon -->
<link rel="shortcut icon" href="assets/images/favicon.ico">

<meta name="csrf-token" content="{{ csrf_token() }}">