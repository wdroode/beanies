<div class="navbar">

    <div class="navbar-header">
        <div class="container">

            <a class="navbar-brand" href="#promo-video"><img src="assets/images/logo_beanies_v2.svg" class="logo" alt="Beanies logo"></a>

            <a class="navbar-toggle btn responsive-menu pull-right" data-toggle="collapse" data-target=".navbar-collapse"><i class='icon-menu-1'></i></a>

        </div>
    </div>

    <div class="yamm">
        <div class="navbar-collapse collapse">
            <div class="container">

                <a class="navbar-brand" href="#promo-video"><img src="assets/images/logo_beanies_v2.svg" class="logo" alt=""></a>

                <ul class="nav navbar-nav">
                    @foreach($targets as $target)
                        <li><a href="#{{ $target }}" class="dropdown-toggle js-activated disabled">{{ $target }}</a></li>
                    @endforeach
                </ul>

            </div>
        </div>
    </div>
</div>