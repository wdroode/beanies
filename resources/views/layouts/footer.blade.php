<div class="footer-bottom">
    <div class="container inner">
        <p class="pull-left">© 2018 Beanies. All rights reserved.</p>
        <ul class="footer-menu pull-right">
            @foreach($targets as $target)
                <li><a href="#{{ $target }}">{{ $target }}</a></li>
            @endforeach
        </ul>
    </div>
</div>