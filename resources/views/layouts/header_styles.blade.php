<!-- Core CSS -->
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/main.css" rel="stylesheet">

<!-- AddOn/Plugin CSS -->
<link href="assets/css/green.css" rel="stylesheet" title="Color">
<link href="assets/css/owl.carousel.css" rel="stylesheet">
<link href="assets/css/owl.transitions.css" rel="stylesheet">
<link href="assets/css/animate.min.css" rel="stylesheet">
<link href="assets/css/aos.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="assets/css/custom.css" rel="stylesheet">