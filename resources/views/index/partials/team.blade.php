<section id="team" class="light-bg">
    <div class="container inner-top inner-bottom-sm">

        <div class="row">
            <div class="col-md-8 col-sm-10 center-block text-center">
                <header>
                    <h1>{{ $articles['beanies']['title'] }}</h1>
                    <p>{{ $articles['beanies']['title'] }}</p>
                </header>
            </div>
        </div>

        <div class="row inner-top-sm text-center">

            <div class="col-sm-4 inner-bottom-sm inner-left inner-right">
                <figure class="member">

                    <div class="icon-overlay icn-link">
                        <a href="{{ $linkedin['mike'] }}"><img src="assets/images/art/human03.jpg" class="img-circle"></a>
                    </div>

                    <figcaption>
                        <h2>{!! $articles['mike']['title'] !!}</h2>
                        <blockquote>{{ $articles['mike']['body'] }}</blockquote>
                    </figcaption>

                </figure>
            </div>

            <div class="col-sm-4 inner-bottom-sm inner-left inner-right">
                <figure class="member">

                    <div class="icon-overlay icn-link">
                        <a href="{{ $linkedin['feifei'] }}"><img src="assets/images/art/human01.jpg" class="img-circle"></a>
                    </div>

                    <figcaption>
                        <h2>{!! $articles['feifei']['title'] !!}</h2>
                        <blockquote>{{ $articles['feifei']['body'] }}</blockquote>
                    </figcaption>

                </figure>
            </div>

            <div class="col-sm-4 inner-bottom-sm inner-left inner-right">
                <figure class="member">

                    <div class="icon-overlay icn-link">
                        <a href="{{ $linkedin['steven'] }}"><img src="assets/images/art/human05.jpg" class="img-circle"></a>
                    </div>

                    <figcaption>
                        <h2>{!! $articles['steven']['title'] !!}</h2>
                        <blockquote>{{ $articles['steven']['body'] }}</blockquote>
                    </figcaption>

                </figure>
            </div>

        </div>

        <div class="row text-center">

            <div class="col-sm-4 inner-bottom-sm inner-left inner-right">
                <figure class="member">

                    <div class="icon-overlay icn-link">
                        <a href="{{ $linkedin['shuhui'] }}"><img src="assets/images/art/human06.jpg" class="img-circle"></a>
                    </div>

                    <figcaption>
                        <h2>{!! $articles['shuhui']['title'] !!}</h2>
                        <blockquote>{{ $articles['shuhui']['body'] }}</blockquote>
                    </figcaption>

                </figure>
            </div>

            <div class="col-sm-4 inner-bottom-sm inner-left inner-right">
                <figure class="member">

                    <div class="icon-overlay icn-link">
                        <a href="{{ $linkedin['wesley'] }}"><img src="assets/images/art/human04.jpg" class="img-circle"></a>
                    </div>
                    <figcaption>
                        <h2>{!! $articles['wesley']['title'] !!}</h2>
                        <blockquote>{{ $articles['wesley']['body'] }}</blockquote>
                    </figcaption>

                </figure>
            </div>

            <div class="col-sm-4 inner-bottom-sm inner-left inner-right">
                <figure class="member">

                    <div>
                        <a><img src="assets/images/art/human02.jpg" class="img-circle"></a>
                    </div>

                    <figcaption>
                        <h2>{!! $articles['mrbeanie']['title'] !!}</h2>
                        <blockquote>{{ $articles['mrbeanie']['body'] }}</blockquote>
                    </figcaption>

                </figure>
            </div>

        </div>

    </div>
</section>