<section id="features">
    <div class="container inner">
        <div class="row">
            <div class="col-sm-6 inner-right-xs text-right aos-init aos-animate" data-aos="fade-up">
                <figure><img src="assets/images/art/product01.jpg" alt=""></figure>
            </div>
            <div class="col-sm-6 inner-top-xs inner-left-xs aos-init aos-animate" data-aos="fade-up">
                <h1>{{ $articles['digitalwallet']['title'] }}</h1>
                <p>{{ $articles['digitalwallet']['body'] }}</p>
            </div>
        </div>

        <div class="row inner-top-md">
            <div class="col-sm-6 col-sm-push-6 inner-left-xs aos-init aos-animate" data-aos="fade-up">
                <figure><img src="assets/images/art/product02.jpg" alt=""></figure>
            </div>
            <div class="col-sm-6 col-sm-pull-6 inner-top-xs inner-right-xs aos-init aos-animate" data-aos="fade-up">
                <h2>{{ $articles['payment']['title'] }}</h2>
                <p>{!! $articles['payment']['body'] !!}</p>
            </div>
        </div>

        <div class="row inner-top-md">
            <div class="col-sm-6 inner-right-xs text-right aos-init aos-animate" data-aos="fade-up">
                <figure><img src="assets/images/art/product03.jpg" alt=""></figure>
            </div>
            <div class="col-sm-6 inner-top-xs inner-left-xs aos-init aos-animate" data-aos="fade-up">
                <h1>{{ $articles['withdrawmoney']['title'] }}</h1>
                <p>{!! $articles['withdrawmoney']['body'] !!}</p>
            </div>
        </div>

        <div class="row inner-top-md">
            <div class="col-sm-6 col-sm-push-6 inner-left-xs aos-init aos-animate" data-aos="fade-up">
                <figure><img src="assets/images/art/product02.jpg" alt=""></figure>
            </div>

            <div class="col-sm-6 col-sm-pull-6 inner-top-xs inner-right-xs aos-init aos-animate" data-aos="fade-up">
                <h1>{{ $articles['feedback']['title'] }}</h1>
                <p>{{ $articles['feedback']['body'] }}</p>
            </div>
        </div>
    </div>
</section>