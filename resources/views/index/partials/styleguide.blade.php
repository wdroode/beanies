<section id="styleguide" class="dark-bg img-bg-soft img-bg" style="background-image: url(assets/images/art/image-background02.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-9 inner center-block text-center">
                <header>
                    <h1>{{ $articles['styleguide']['title'] }}</h1>
                    <p>{!! $articles['styleguide']['body'] !!}</p>

                    <div class="fadeInDown-3">
                        <a href="{{ asset('assets/pdf/styleguide.pdf') }}" target="_blank" class="btn btn-large">Styleguide PDF</a>
                    </div>
                </header>
            </div>
        </div>
    </div>
</section>