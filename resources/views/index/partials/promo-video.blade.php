<section id="promo-video" class="light-bg">
    <div class="container inner-top-xs inner-bottom">

        <div class="col-md-12">
                <div class="video-container">
                    <iframe id="ytplayer" type="text/html" width="720" height="405" src="https://www.youtube.com/embed/BZTYnJIlGvI?autoplay=1&rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>

    </div>
</section>