<section id="conclusion">
    <div id="owl-main" class="owl-carousel owl-one-item">

        <div class="item dark-bg img-bg img-bg-soft" style="background-image: url(assets/images/art/slider02.png);">
            <div class="container">
                <div class="caption vertical-center text-left">

                    <h1 class="fadeInDown-1 dark-bg light-color" style="opacity: 0;"><span>{{ $articles['conclusion']['title'] }}</span></h1>
                    <p class="fadeInRight-2 light-color" style="opacity: 0; left: -15px;">{{ $articles['conclusion']['body'] }}</p>

                </div>
            </div>
        </div>

    </div>
</section>
