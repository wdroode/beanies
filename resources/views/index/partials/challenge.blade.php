<section id="challenge" class="dark-bg">
    <div class="container inner-md">
        <div class="row inner-top-xs">
                    <div class="col-sm-6 inner-right-xs inner-bottom-xs aos-init aos-animate" data-aos="fade-up">
                        <h2 class="light-color">{{ $articles['challenge']['title'] }}</h2>
                        <p>{!! $articles['challenge']['body'] !!}</p>
                    </div>

                    <div class="col-sm-6 inner-left-xs inner-bottom-xs aos-init aos-animate" data-aos="fade-up">
                        <h2 class="light-color">{{ $articles['aim']['title'] }}</h2>
                        <p>{{ $articles['aim']['body'] }}</p>
                    </div>
            </div>
        </div>
    </div>
</section>