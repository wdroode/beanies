@extends('master')

@section('content')
    @include('index.partials.promo-video')
    @include('index.partials.challenge')
    @include('index.partials.process')
    @include('index.partials.styleguide')
    @include('index.partials.features')
    @include('index.partials.conclusion')
    @include('index.partials.team')
@endsection