# Beanies - Case Study

This website represents our case study for the 'Designing Digital Experiences' project.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install and how to install them

```
Composer
Laravel 5.5
```

### Installing

For installing composer see:
https://getcomposer.org/doc/00-intro.md

For Laravel documentation go to:
https://laravel.com/docs/5.5/installation

To install the project:

Clone the project
```
https://wdroode@bitbucket.org/wdroode/beanies.git
```

Change to your working directory
```
cd ../project directory/
```

Run composer install
```
composer install
```

## Running the tests

Because this is a static website, testing will not be required.

### And coding style tests

PSR-2

For the complete style guide refer to:
```
https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md
```

## Deployment

Deployment will be handled by Wderoode

## Built With

* [Laravel](https://laravel.com/docs/5.5) - The web framework used
* [Composer](https://getcomposer.org/) - Dependency Management

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **Wesley de Roode** - *Initial work* - [wderoode.nl](www.wderoode.nl)

See also the list of [contributors](https://bitbucket.org/wdroode/beanies) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
