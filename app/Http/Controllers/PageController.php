<?php


namespace App\Http\Controllers;

class PageController extends Controller
{
    /**
     * Display a listing of articles.
     * @return \Illuminate\Http\Response
     * @internal param Category $category
     * @internal param ArticleFilter $filters
     */
    public function index()
    {
        return view('index.index', [
            'linkedin' => $this->linkedin(),
            'targets'  => $this->targets(),
            'articles' => $this->articles(),
        ]);
    }

    /**
     * Get PDF by name.
     * @param $name string
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function getPDF($name)
    {
        $pdf = public_path() . "/assets/pdf/" . $name . '.pdf';

        return \response()->download($pdf);
    }

    /**
     * Linkedin profile url's
     * @return array of strings
     */
    private function linkedin()
    {
        return [
            'wesley' => 'https://www.linkedin.com/in/wesley-de-roode-8144b3a3/',
            'steven' => 'https://www.linkedin.com/in/steven-van-der-stel-a2465a110/',
            'shuhui' => 'https://www.linkedin.com/in/shuhui/',
            'feifei' => 'https://www.linkedin.com/in/feifei-zhou-725737100/',
            'mike'   => 'https://www.linkedin.com/in/mike-schouten-bb9ba5a9/',
        ];
    }

    /**
     * Page targets.
     */
    private function targets()
    {
        return [
            'challenge',
            'process',
            'features',
            'conclusion',
            'team',
        ];
    }

    /**
     * Articles posted on the page.
     * @return array of strings
     */
    public function articles()
    {
        return [
            'challenge'        => [
                'title' => 'Challenge',
                'body'  => 'Design agency <b><a href=vanberlo target="_blank">VanBerlo</a></b> assigned us to a challenge. This challenge was to design a payment system that is inclusive and geared towards all different kinds of users who are visually impaired or blind. VanBerlo is currently working on their own solution and products, but would like to receive new and innovative insights and ideas. This is where we came in to contribute and help.',
            ],
            'aim'              => [
                'title' => 'Aim',
                'body'  => 'The goal was to develop a working prototype that meets the minimal needs of those who are blind and visually impaired. The working prototype should be easy to use for everyone, give reliable feedback and have considerable advantages over the current wireless payment system by Worldline in the Netherlands. Our scope remained within the Netherlands among the Dutch citizens.',
            ],
            'process'          => [
                'title' => 'Process',
                'body'  => 'We worked with agile methodologies and each of us played a significant role in every step of the proces.',
            ],
            'technology'       => [
                'title' => 'Technology',
                'body'  => 'We researched technologies that are currently assisting people with impaired vision in daily life. Things like braille en text to speech are the obvious options that came into mind. We also looked into more futuristic technologies like a braille touch screen and finger reader.',
            ],
            'atmfieldresearch' => [
                'title' => 'ATM field research',
                'body'  => 'At the start we did not yet know what our focus would be for the final prototype so we decided to do some field research. We tested payment terminals and ATMs from an interaction point of view, and tried to pinpoint possible bottlenecks for the visually impaired.',
            ],
            'userresearch'     => [
                'title' => 'User research',
                'body'  => 'For our application to be useful for the blind and visually impaired we needed to understand their needs and wants. Therefore we interviewed several people from this target group and tested our prototype in different stages with them. The results leaded to refreshing insights.',
            ],
            'walley'           => [
                'title' => 'Walley',
                'body'  => 'Walley is an app that makes it possible to pay with your iPhone. You no longer need a debit card or use your pin code. Instead, payments can easily be made with biometric verification on the iPhone. An iPhone app would fit the target audience because it offers accessibility features. These allow the blind and visually impaired to navigate through apps with minimum errors.',
            ],
            'braillewatch'     => [
                'title' => 'Braille watch',
                'body'  => 'The braille watch makes use of two technologies. A relative new braille smartwatch technology and NFC technology. This concept focuses on payment with NFC and quick and small braille feedback. Users will be able to read things like the price and whether the payment was successful or not.',
            ],
            'futuristicvision' => [
                'title' => 'Futuristic vision',
                'body'  => 'The final concept is based on a vision we have for the future of payment transactions. Payment terminals will be gone and physical money and debit cards are also no longer needed. The payment will be sent out in a local area and can be received and accepted on your personal device. At this point in time this device will be your smartphone but this could change in the future.',
            ],
            'ux'               => [
                'title' => 'UX',
                'body'  => 'We focussed on a few design guidelines to give our users the best experience: <ul class="arrowed"><li>Our application needs to give clear feedback, so that every kind of user can easily use it to make payments.</li><li>The design needs to be kept simple, as it is especially challenging for blind people to locate something when there is too much information.</li><li>We should include a functionality that makes it easy to withdraw money, since currently this seems an impossible task for blind and visually impaired people.</li></ul>',
            ],
            'visual'           => [
                'title' => 'Visual',
                'body'  => 'Choosing the right visual identity for our application was a challenging task, because the visually impaired have different needs. We wanted the application to feel familiar, secure and calm. To gather insights, we approached the visually impaired and people with normal sight to fill in a survey about their color preferences. The color blue seemed the preferred color for both target groups, and fits the look and feel that we wanted.',
            ],
            'iosdevelopment'   => [
                'title' => 'iOS development',
                'body'  => 'Our computer science team members developed a working iOS application as final prototype. Developing this app was quite challenging, since they were unacquainted with the programming language Swift. Therefore, our goal was to at least finish the core functionality - making a wireless payment with biometric verification. The development had its highs and lows, but in the end the whole team is satisfied with the result.',
            ],
            'styleguide'       => [
                'title' => 'Styleguide',
                'body'  => "As the brand colors we have chosen for blue and white. This color combination has a clear contrast that will make things easier to see for the visually impaired. Also, the color blue generally gives people a safe and trusted feeling, something that is important when you are handling other people's money. <br><br>The body text is black on white in the font San Francisco. San Francisco works best with the accessibility features on the iPhone. The color combinations, together with clear icons, and enough white space should help making Walley more user-friendly.",
            ],
            'digitalwallet'    => [
                'title' => 'Your digital wallet',
                'body'  => 'Walley turns your mobile phone into a payment device. The app can store multiple debit cards, save all your receipts, and plan your money withdrawal beforehand in your own safe environment. No hassle in finding your debit cards, losing your receipts, or long queues at the ATMs. Using your PIN is not required, just verify with secure biometrics.',
            ],
            'payment'          => [
                'title' => 'Secure & fast payment',
                'body'  => 'Walley replaces all your debit cards. Within the app you can select a default bank account to use for payment. Making a payment is as following: <ul class="arrowed"><li>Open Walley</li><li>Put your smartphone onto the NFC chip of the payment terminal</li><li>Verify the payment with biometrics or a digit code</li>',
            ],
            'withdrawmoney'    => [
                'title' => 'Easily withdraw money',
                'body'  => 'Walley keeps your money withdrawal plan until you decide to pick up the money later. Planning your money withdrawal is very easy and can be done in your own safe environment:  <ul class="arrowed"><li>Open Walley</li><li>Choose ‘Geld opnemen’</li><li>Type or tell Walley what amount you want to collect</li></ul>',
            ],
            'feedback'         => [
                'title' => 'Always feedback',
                'body'  => 'Walley will always provide proper feedback whether you are paying at the cashier or withdrawing money at the bank. This feedback consists of the usual toasts, haptic feedback and a voice-over for the visually impaired.',
            ],
            'conclusion'       => [
                'title' => 'Conclusion',
                'body'  => 'After testing our prototype with our target group, we can conclude that our product has real potential to improve the hassle of transactions. The product itself is a good start, but a all-in-one banking solution would be even more convenient for our users. Therefore we would like to collaborate with a bank like ABN Amro for example, to implement our concept in a existing banking app.',
            ],
            'beanies'          => [
                'title' => 'The people behind the Beanies',
                'body'  => 'We are a multidisciplinary team of 5 students from the Rotterdam University of Applied Sciences. Our passion is to build digital products with the mindset of improving people’s daily life.',
            ],
            'mike'             => [
                'title' => 'Mike Schouten <span>Developer</span>',
                'body'  => 'Not the hero we need but the hero we deserve.',
            ],
            'steven'           => [
                'title' => 'Steven van der Stel <span>Visual Designer</span>',
                'body'  => 'Likes to draw things and eat jelly beans while doing it.',
            ],
            'wesley'           => [
                'title' => 'Wesley de Roode <span>Developer & Interaction Designer</span>',
                'body'  => 'When doing IT stuff people tend to think I do magic. Unlike Feifei, I guess I am a magician.',
            ],
            'shuhui'           => [
                'title' => 'Shuhui Shi <span>User researcher & Visual Designer</span>',
                'body'  => 'Choose a job you love, and you will never have to work a day in your life.',
            ],
            'feifei'           => [
                'title' => 'Feifei Zhou <span>Interaction Designer</span>',
                'body'  => 'I\'m a designer, not a magician. Although that would be cool.',
            ],
            'mrbeanie'         => [
                'title' => 'Mr. Beanie <span>Founder & CEO</span>',
                'body'  => 'Although strongly silent, has a huge impact and is a rolemodel for all Beanies.',
            ],
        ];
    }

}

